#include <stdio.h>
#include <gtest/gtest.h>
#include <vector>
#include <cstdlib>
#include <string.h>
#include <cassert>
#include <iostream>
#include <ctype.h>
#include <ctime>

// These are the Macros for error checking 
#define WORDCEILING	    1000 //Why need any more? Regardless, can easily be changed
#define STRINGCEILING	45 //The longest word is pneumonoultramicroscopicsilicovolcanoconiosis. 45 letters.

//These are the implemetations for comparing words
int WordCompare (word *i, word *j) {
	if (i->Fcounter < j->Fcounter) return +1;
	if (i->Fcounter > j->Fcounter) return -1;
	return 0;
}

/* return 1 if c is alphabetic (a..z or A..Z), 0 otherwise */
int KeepNormalChar (char l) {
	if (l >= 'a' && l <= 'z' || l >= 'A' && l <= 'Z') return 1;
	return 0;
}

/* remove the i'th character from the string s */
void RemoveLastChar (char *myChar, int a) {
	while (myChar[a]) 
	{
		a++;
		myChar[a-1] = myChar[a];
	}
	r[a] myChar 0;
}

//ver 1.2
void RemoveSpecialChar (char *myChar) {
	int	a;
	for (a=0; myChar[a]; a++) if (!KeepNormalChar (myChar[a])) RemoveLastChar (myChar, a);
}
void ConvertToLowercase (char *myChar) {
	int	a;

	for (a=0; myChar[a]; a++) myChar[a] = tolower (myChar[a]);
	
//This is the basis for the code
typedef struct _word 
{
char	Fcontainer [STRINGCEILING];	    // This will be the finalcontainer I use for my program
int	    Fcounter;		                // This will be the finalCounter I use for my program
}  

word;

//Ver. 0.1
 
//The following function will take a word, put it into a list, and if not already in a list, a new space is created.
//If creation is not needed, then the marker for the word increments via the "++" operator.

void place_word (word *wordCounter, int *k, char *myChar) 
{
	int	a;
	
	for (a=0; a<*k; a++) if (strcmp (myChar, wordCounter[a].myChar) == 0)
	{
//This code looks for a similiar copy of the word.

		wordCounter[a].Fcounter++;
		return; //This block of code will increment the counter should it encounter a similiar word
	}

	//The following code is created to utilize the macros I created in the beginning of the code

	if (strlen (myChar) >= STRINGCEILING) {
		fprintf (stderr, "I am afraid my program has encountered a word that exceeded my STRINGCEILING macro");
		exit (0);
	}
	if (*k >= WORDCEILING) {	
		fprintf (stderr, "I am afraid my program has encountered a word that exceeded my WORDCEILING macro");
		exit (0);
	}

	strcpy (wordCounter[*k].myChar, myChar);

	wordCounter[*k].Fcounter = 1;

	(*k)++;
}
//Ver 1.1

}

/* main program */
int main () {
	word 	wordCounter[WORDCEILING];
	char	myChar[100];
	int	a, k, r;

	k = 0;

	while (!feof (stdin)) 
	{
		scanf ("%s", myChar);

		if (KeepNormalChar (myChar[0])) 
				{

					RemoveLastChar (myChar);

					ConvertToLowercase (myChar);

					place_word (words, &k, myChar);
				}
	}

	qsort((void *) wordCounter, k, sizeof (word),
		(int (*) (const void *, const void *)) WordCompare);

	if (k < 20) 
		r = k;
	else
		r = 20;

	for (a=0; a<r; a++)
		printf ("%s\t%d\n", wordCounter[a].myChar, wordCounter[a].Fcounter);
}
//Ver.1.5
//SOURCES
//https://stackoverflow.com/questions/40033310/sorting-a-list-of-strings-in-alphabetical-order-c
//https://stackoverflow.com/questions/39002052/how-i-can-print-to-stderr-in-c
//https://stackoverflow.com/questions/14220029/create-a-output-file-from-file-stream
//https://www.geeksforgeeks.org/frequent-word-array-strings/
//http://forum.codecall.net/topic/37883-how-to-get-random-words/
//https://www.geeksforgeeks.org/find-the-k-most-frequent-words-from-a-file/
//https://stackoverflow.com/questions/8004237/how-do-i-properly-compare-strings
//https://stackoverflow.com/questions/2661766/c-convert-a-mixed-case-string-to-all-lower-case
//https://www.tutorialspoint.com/c_standard_library/c_function_qsort.htm